<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\Parachute;

class WebController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template("Web/index.html.twig")
     */
    public function indexAction(Request $request, Parachute $Parachute)
    {
        $Parachute->message("jordan", "hello there");

        return array();
    }

    /**
     * @Route("/callback", name="callback")
     * @Template("Web/index.html.twig")
     */
    public function callbackAction(Request $request)
    {
        error_log(json_encode($request));

        return array();
    }
}
