<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\AppUser;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
	
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $roleRepository = $this->container->get('doctrine')->getRepository('AppBundle:AppRole');
        $role = $roleRepository->findOneByRole("ROLE_SUPER_ADMIN");

        $user = new AppUser();
        $user->setUsername("jordan");
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $user->setPassword($encoder->encodePassword('password', $user->getSalt()));
        $user->setEmail("jordan@gmail.com");
        $user->addRole($role);

        $manager->persist($user);
        $manager->flush();
        
        $roleRepository = $this->container->get('doctrine')->getRepository('AppBundle:AppRole');
        $role = $roleRepository->findOneByRole("ROLE_USER");
        
        $user = new AppUser();
        $user->setUsername("user");
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $user->setPassword($encoder->encodePassword('password', $user->getSalt()));
        $user->setEmail("user@gmail.com");
        $user->addRole($role);

        $manager->persist($user);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}