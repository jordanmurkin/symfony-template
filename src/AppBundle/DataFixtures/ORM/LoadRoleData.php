<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\AppRole;

class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $role = new AppRole();
        $role->setName("SuperAdmin");
        $role->setRole("ROLE_SUPER_ADMIN");
	  
        $manager->persist($role);
        $manager->flush();

        $role = new AppRole();
        $role->setName("Admin");
        $role->setRole("ROLE_ADMIN");

        $manager->persist($role);
        $manager->flush();

        $role = new AppRole();
        $role->setName("User");
        $role->setRole("ROLE_USER");

        $manager->persist($role);
        $manager->flush();
    }
	
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}