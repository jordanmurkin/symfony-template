<?php
namespace AppBundle\Service;

class Parachute
{
    /** API.ai configuration **/
    private $apiUrl = "https://parachute.webnicol.fr/developer/message";
    private $developerToken = "$2y$10$/QvXxf.WCZ6MbbHuJ9y5cOIaiy2vCEVqDm/gVjcnruCR8c1P4MYoO";

    /** Class variables **/
    protected $httpRequest;

    public function __construct(HttpRequest $httpRequest)
    {
        $this->httpRequest = $httpRequest;
    }

    public function message(string $user, string $message)
    {
        $url = $this->apiUrl;

        $data = array(
            'developer_token' => $this->developerToken,
            'user_name' => $user,
            'user_message' => $message
        );
        $json = json_encode($data);

        $result = $this->httpRequest->postJson($url, $json);
        return json_decode($result, true);
    }
}
