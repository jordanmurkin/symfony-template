composer install
sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX var/cache var/logs var/sessions
sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx var/cache var/logs var/sessions
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load
php bin/console cache:clear
php bin/console cache:clear --env=prod

